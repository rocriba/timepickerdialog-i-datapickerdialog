package cat.dam.rocriba.horoscop;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;


import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    // Afegim variables
    TextView tv_data, tv_hora, tv_edat, tv_animal, tv_horoscop;
    DatePickerDialog selectorData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Indicar Hora
        tv_hora = (TextView) findViewById(R.id.tv_hora);
        tv_hora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendari = Calendar.getInstance();
                int horaActual = calendari.get(Calendar.HOUR_OF_DAY);
                int minutActual = calendari.get(Calendar.MINUTE);
                int horaActualr = calendari.get(Calendar.HOUR_OF_DAY);
                int minutActualr = calendari.get(Calendar.MINUTE);
                TimePickerDialog selectorTemps;
                selectorTemps = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker selectorTemps, int horaSeleccionada, int minutSeleccionat){
                        tv_hora.setText((String.format("%02d:%02d", horaSeleccionada, minutSeleccionat)));
                    }
                },horaActual, minutActual, true);
                selectorTemps.setTitle("Seleccioni l'hora de neixament");
                selectorTemps.show();
            }
        });

        //Indica data
        //iIncialitzem i indeixem els Textview amb les variables
        tv_data = (TextView) findViewById(R.id.tv_data);
        tv_edat = (TextView) findViewById(R.id.edat);
        tv_animal = (TextView) findViewById(R.id.animal);
        tv_horoscop = (TextView) findViewById(R.id.horoscop);
        tv_data.setOnClickListener(  new View.OnClickListener() {

            //Sobrescribim i inicilitzalem les variables que ens indicaran quin mes any etc estem i lles altres guardaran les dades del usuari.
            @Override
            public void onClick(View v) {
                final Calendar calendari = Calendar.getInstance();
                int any = calendari.get(Calendar.YEAR);
                int mes = calendari.get(Calendar.MONTH);
                int mesr = calendari.get(Calendar.MONTH);
                int dia = calendari.get(Calendar.DAY_OF_MONTH);
                int diar = calendari.get(Calendar.DAY_OF_MONTH);


                //Quan selecioni la data el programa fara la resta de parts de l'exercici, calcularà l¡horoscop, edat i animal
                selectorData = new DatePickerDialog(MainActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int any,
                                                  int mes, int dia) {

                                tv_data.setText(dia + "/"
                                        + (mes + 1) + "/" + any);



                                int edd = 2020-any;
                                tv_edat.setText("Edat:" + edd);

                               if (any == 2008 || any == 1996 || any == 1984 || any== 1972 || any == 1960){
                                   tv_animal.setText("Animal Rata");
                               } else if (any == 2009 || any == 1997 || any == 1985 || any ==1985 || any == 1973 || any == 1961){
                                   tv_animal.setText("Animal Buey");
                               } else if (any == 2010 || any == 1998 || any == 1986 || any ==1974 || any == 1962) {
                                   tv_animal.setText("Animal Tigre");
                               } else if (any == 2011 || any == 1999 || any == 1987 || any == 1975 || any == 1963) {
                                   tv_animal.setText("Animal Conill");
                               } else if ( any == 2012 || any == 2000 || any == 1988 || any == 1976 || any == 1964) {
                                   tv_animal.setText("Animal Drac");
                               } else if (any == 2013 ||any == 2001 || any== 1989 || any == 1977 || any== 1965 ) {
                                   tv_animal.setText("Animal Serp");
                               } else if (any == 2014 || any == 20002 || any == 1990 || any == 1978 || any == 1966){
                                   tv_animal.setText("Animal caball");
                               } else if (any == 2015 || any == 2003 || any == 1991 || any == 1979 || any == 1967) {
                                   tv_animal.setText("Animal Cabra");
                               } else if (any == 2016 || any == 2004 || any == 1992 || any == 1980 || any == 1968){
                                   tv_animal.setText("Animal Mono");
                               } else if (any == 2017 || any == 2005 || any == 1993 || any == 1981 || any == 1969){
                                   tv_animal.setText("Animal Gall");
                               } else if (any == 2018 || any == 2006 || any == 1994 || any == 1982 || any == 1970){
                                   tv_animal.setText("Animal Gos");
                               } else if (any == 2007 || any == 1995 || any == 1983 || any == 1971 || any == 1971){
                                   tv_animal.setText("Animal Porc");
                               } else{
                                   tv_animal.setText("Any entrat incorrecte");
                               }


                              switch (mes +1) {
                                  case 1:
                                      if (dia >= 1 && dia <=19){
                                          tv_horoscop.setText("Capricorn");
                                      } else if (dia > 19) {
                                          tv_horoscop.setText("Aquaris");

                                  } else {
                                          tv_horoscop.setText("Error 1");
                                      }
                                      break;
                                  case 2:
                                      if (dia >=1 && dia <=19){
                                          tv_horoscop.setText("Aquaris");
                                      }else if (dia >= 20){
                                          tv_horoscop.setText("Piscis");
                                      } else{
                                          tv_horoscop.setText("Error 2");
                                      }
                                      break;
                                  case 3:
                                      if (dia >=1 && dia <=20){
                                          tv_horoscop.setText("Piscis");
                                      }else if (dia >=21){
                                          tv_horoscop.setText("Aries");
                                      } else {
                                          tv_horoscop.setText("Error 3");
                                      }
                                      break;
                                  case 4:
                                      if (dia >=1 && dia <=20) {
                                          tv_horoscop.setText("Aries");
                                      }else if (dia >=21){
                                          tv_horoscop.setText("Tauro");
                                      } else {
                                          tv_horoscop.setText("Error 4");
                                      }
                                      break;
                                  case 5:
                                      if (dia >=1 && dia <=21){
                                          tv_horoscop.setText("Tauro");
                                      } else if (dia >=21){
                                          tv_horoscop.setText("Geminis");
                                      } else {
                                          tv_horoscop.setText("Error 5");
                                      }
                                      break;
                                  case 6:
                                      if (dia >=1 && dia <= 21){
                                          tv_horoscop.setText("Geminis");
                                      } else if (dia >= 22) {
                                          tv_horoscop.setText("Cancer");
                                      } else {
                                          tv_horoscop.setText("Error 6");
                                      }
                                      break;
                                  case 7:
                                      if (dia >=1 && dia <=22){
                                          tv_horoscop.setText("Cancer");
                                      } else if (dia >=23) {
                                          tv_horoscop.setText("Leo");
                                      } else {
                                          tv_horoscop.setText("Error 7");
                                      }
                                      break;
                                  case 8:
                                      if (dia >= 1 && dia <= 22){
                                          tv_horoscop.setText("Leo");
                                      } else if (dia>=23) {
                                          tv_horoscop.setText("Virgo");
                                      } else {
                                          tv_horoscop.setText("Error 8");
                                      }
                                      break;
                                  case 9:
                                      if (dia>=1 && dia <=22) {
                                          tv_horoscop.setText("Virgo");
                                      } else if (dia >= 23) {
                                          tv_horoscop.setText("Libra");
                                  } else {
                                          tv_horoscop.setText("Error 9");
                                      }
                                      break;
                                  case 10:
                                      if (dia >=1 && dia <= 22){
                                          tv_horoscop.setText("Libra");
                                      } else if(dia >=23) {
                                      tv_horoscop.setText("Scorpio");
                                  } else {
                                          tv_horoscop.setText("Error 10");
                                      }
                                      break;
                                  case 11:
                                      if (dia >=1 && dia <=21) {
                                          tv_horoscop.setText("Scorpio");
                                      } else if (dia >=22){
                                          tv_horoscop.setText("Sagitari");
                                      } else {
                                          tv_horoscop.setText("Error 11");}
                                      break;
                                  case 12:
                                      if (dia >=1 && dia <= 21){
                                          tv_horoscop.setText("Sagitari");
                                      } else if (dia >= 22){
                                          tv_horoscop.setText("Capricorn");
                                      } else {
                                          tv_horoscop.setText("Error 12");
                                      }
                                      break;
                                  default:
                                      tv_horoscop.setText("Error general");
                                      break;
                              }


                            }
                        }, any, mes, dia);
                selectorData.show();
            }
        });



            }

}